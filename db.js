  var sql = require('./node_modules/sql.js/js/sql-debug.js');
  var fs = require("fs");
  //Ditto, path module
  var path = require('path');

  try {
  	var filebuffer = fs.readFileSync(path.join(__dirname, 'quinche.sqlite'));
  } catch (e) {
  	console.log(e);
  }

  if (filebuffer != null) {
  	var db = new SQL.Database(filebuffer);
  } else {
  	var db = new SQL.Database();
  }

  try {
  	db.run("CREATE TABLE IF NOT EXISTS USUARIO (id_usuario primary key not null,nombre varchar(50), apellido varchar(50), imagen varchar(50));");
  } catch (e) {
  	console.log(e);
  }
  
  try {
  	db.run("CREATE TABLE IF NOT EXISTS EVENTO_TAREAS (id_evento INT PRIMARY KEY NOT NULL,nombre_evento VARCHAR(50),detalle_evento TEXT,fechainicio_evento DATE,fechafin_evento DATE,horafin_evento DATE,prioridad_evento VARCHAR(10),color_evento VARCHAR(50),adjunto_evento TEXT,progreso_evento INT);");
  } catch (e) {
  	console.log(e);
  }

  try {
  	db.run("CREATE TABLE IF NOT EXISTS RECORDATORIO (id_recordatorio int primary key not null,id_evento int,intervalo_recordatorio int, formato_intervalo VARCHAR(10), FOREIGN KEY(id_evento) REFERENCES EVENTO_TAREAS (id_evento));");
  } catch (e) {
  	console.log(e);
  }
  try {
  	db.run("CREATE TABLE IF NOT EXISTS URLS (id_url int primary key not null,id_evento int,url text,detalle url varchar(200), foreign key(id_evento) references evento_tareas (id_evento));");
  } catch (e) {
  	console.log(e);
  }
 
  
  db.each("SELECT * FROM USUARIO", function(row) {
  	console.log(row.id_usuario + " es de los usuarios!"+row.nombre+"nombre"+"apellido"+row.apellido+row.imagen);
  });

  try {
  	fs.unlinkSync(path.join(__dirname, 'quinche.sqlite'));
  } catch (e) {
  	console.log("Unable to delete file; Exception: " + e);
  }
  fs.writeFileSync("quinche.sqlite", new Buffer(db.export()));
